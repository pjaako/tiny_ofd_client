//
// Created by pgromovikov on 3/31/2019.
//



#ifndef TINYOFDCLIENT_TCP_CLIENT_H
#define TINYOFDCLIENT_TCP_CLIENT_H
int tcp_send_receive(const char * IP_ADDR, uint16_t PORT_NUM, int timeout_ms, uint8_t * data_buffer, int data_len);
#endif //TINYOFDCLIENT_TCP_CLIENT_H

